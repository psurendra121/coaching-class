# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  rolify

  has_one :subject
  has_one :profile

  after_create :assign_default_role

  def assign_default_role
    add_role(:student) if roles.blank?
  end
end
