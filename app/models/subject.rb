# frozen_string_literal: true

class Subject < ApplicationRecord
  belongs_to :User
end
