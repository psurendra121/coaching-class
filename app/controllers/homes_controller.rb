# frozen_string_literal: true

class HomesController < ApplicationController
  def index
    redirect_to '/profiles/<%= current_user.id %>' if user_signed_in?
  end
end
