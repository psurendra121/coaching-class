# frozen_string_literal: true

class ProfilesController < ApplicationController
  def new
    @profile = Profile.new
  end

  def show
    @profile = current_user.profile
  end

  def create
    @user = User.find(current_user.id)
    @profile = @user.create_profile(profile_params)
    if @profile.save
      redirect_to root_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @profile = current_user.profile
  end

  def update
    @profile = current_user.profile
    if @profile.update(profile_params)
      redirect_to root_path
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private

  def profile_params
    params.require(:profile).permit(:name, :father_name, :dob, :avatar, :age)
  end
end
