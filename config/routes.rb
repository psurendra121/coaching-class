# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'homes#index'
  get '/setuser', to: 'homes#set_role'

  resources :profiles
  # get 'profiles/:user_id', to: 'profiles#show'
  # get 'profiles/new', to: 'profiles#new'
  # post 'profiles/:user_id', to: 'profiles#create'
  # get 'profiles/:user_id/edit', to: 'profiles#edit'
  # put 'profiles/:user_id', to: 'profiles#update'
end
